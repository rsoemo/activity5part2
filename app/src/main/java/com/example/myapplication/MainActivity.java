package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    Button btn_Roll;
    TextView tv_RollNumber;
    ImageView iv_Dice;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Roll = (Button) findViewById(R.id.btn_Roll);
        tv_RollNumber = (TextView) findViewById(R.id.tv_RollNumber);
        iv_Dice = (ImageView) findViewById(R.id.iv_Dice);

        btn_Roll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                Random r = new Random();
                int theRoll;
                theRoll = r.nextInt(6) + 1;
                tv_RollNumber.setText("You rolled a " + theRoll);

                switch (theRoll)
                {
                    case 1:
                    {
                        iv_Dice.setImageResource(R.drawable.dice1);
                        break;
                    }
                    case 2:
                    {
                        iv_Dice.setImageResource(R.drawable.dice2);
                        break;
                    }
                    case 3:
                    {
                        iv_Dice.setImageResource(R.drawable.dice3);
                        break;
                    }
                    case 4:
                    {
                        iv_Dice.setImageResource(R.drawable.dice4);
                        break;
                    }
                    case 5:
                    {
                        iv_Dice.setImageResource(R.drawable.dice5);
                        break;
                    }
                    case 6:
                    {
                        iv_Dice.setImageResource(R.drawable.dice6);
                        break;
                    }
                }
            }
        });
    }
}